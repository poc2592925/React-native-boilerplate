<div align="center">
    <img src="documentation/static/img/What-Is-React-Native.png" alt="Logo" width="100%">
</div>

## Requirements

Node 18 or greater is required. Development for iOS requires a Mac and Xcode 10 or up, and will target iOS 11 and up.

You also need to install the dependencies required by React Native.  
Go to the [React Native environment setup](https://reactnative.dev/docs/environment-setup), then select `React Native CLI Quickstart` tab.  
Follow the instructions for your given `development OS` and `target OS`.

## Quick start

To create a new project using the boilerplate simply run :

```
- npx react-native@latest init MyApp --template https://gitlab.com/poc2592925/React-native-boilerplate
- cd MyApp
- Yarn Start
- Yarn <platform>
```